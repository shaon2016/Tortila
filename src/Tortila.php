<?php
/**
 * Created by PhpStorm.
 * User: Shaon
 * Date: 10/15/2016
 * Time: 3:14 PM
 */

namespace App;

use App\Message;
use App\Utility;

class Tortilla
{
    public $id;

//    ROTI 6p20tk

    private $distributedPacketForRoti6p20tk;
    private $bonusForRoti6p20tk;
    private $replaceForRoti6p20tk;
    private $returnForRoti6p20tk;
    private $totalSalePacketForRoti6p20tk;
    private $salesInAmountForRoti6p20tk;
    private $extSaleForRoti6p20tk;
    private $totalSaleAmountForRoti;
    private $dueReceivedForRoti;
    private $dueInAmountForRoti;
    private $expAndComForRoti;
    private $totalReceivedForRoti;

    //Roti 4p14tk

    private $distributedPacketForRoti4p14tk;
    private $bonusForRoti4p14tk;
    private $replaceForRoti4p14tk;
    private $returnForRoti4p14tk;
    private $totalSalePacketForRoti4p14tk;
    private $salesInAmountForRoti4p14tk;
    private $extSalesForRoti4p14tk;

//    DONUT 2p18tk

    private $distributedPacketForDonut2p18tk;
    private $bonusForDonut2p18tk;
    private $replaceForDonut2p18tk;
    private $returnForDonut2p18tk;
    private $totalSalePacketForDonut2p18tk;
    private $salesInAmountForDonut2p18tk;
    private $extSaleForDonut2p18tk;
    private $totalSaleAmountForDonut;
    private $dueReceivedForDonut;
    private $dueInAmountForDonut;
    private $expAndComForDonut;
    private $totalReceivedForDonut;

    //Donut 3p@22

    private $distributedPacketForDonut3p22tk;
    private $bonusForDonut3p22tk;
    private $replaceForDonut3p22tk;
    private $returnForDonut3p22tk;
    private $totalSalePacketForDonut3p22tk;
    private $salesInAmountForDonut3p22tk;
    private $extSalesForDonut3p22tk;


//    SAMOSA (BEEF) 24p@45

    private $distributedPacketForSamosaBeef24p45tk;
    private $bonusForSamosaBeef24p45tk;
    private $replaceForSamosaBeef24p45tk;
    private $returnForSamosaBeef24p45tk;
    private $totalSalePacketForSamosaBeef24p45tk;
    private $salesInAmountForSamosaBeef24p45tk;
    private $extSaleForSamosaBeef24p45tk;
    private $totalSaleAmountForSamosaBeef;
    private $dueReceivedForSamosaBeef;
    private $dueInAmountForSamosaBeef;
    private $expAndComForSamosaBeef;
    private $totalReceivedForSamosaBeef;

    //Samosa  Beef   12p45tk

    private $distributedPacketForSamosaBeef12p45tk;
    private $bonusForSamosaBeef12p45tk;
    private $replaceForSamosaBeef12p45tk;
    private $returnForSamosaBeef12p45tk;
    private $totalSalePacketForSamosaBeef12p45tk;
    private $salesInAmountForSamosaBeef12p45tk;
    private $extSalesForSamosaBeef12p45tk;


//    Samosa Chicken 24p80tk

    private $distributedPacketForSamosaChicken24p80tk;
    private $bonusForSamosaChicken24p80tk;
    private $replaceForSamosaChicken24p80tk;
    private $returnForSamosaChicken24p80tk;
    private $totalSalePacketForSamosaChicken24p80tk;
    private $salesInAmountForSamosaChicken24p80tk;
    private $extSaleForSamosaChicken24p80tk;
    private $totalSaleAmountForSamosaChicken;
    private $dueReceivedForSamosaChicken;
    private $dueInAmountForSamosaChicken;
    private $expAndComForSamosaChicken;
    private $totalReceivedForSamosaChicken;

    //Samosa Chicken   12p45

    private $distributedPacketForSamosaChicken12p45;
    private $bonusForSamosaChicken12p45;
    private $replaceForSamosaChicken12p45;
    private $returnForSamosaChicken12p45;
    private $totalSalePacketForSamosaChicken12p45;
    private $salesInAmountForSamosaChicken12p45;
    private $extSalesForSamosaChicken12p45;


//    SINGARA 18p@50

    private $distributedPacketForSingara18p50tk;
    private $bonusForSingara18p50tk;
    private $replaceForSingara18p50tk;
    private $returnForSingara18p50tk;
    private $totalSalePacketForSingara18p50tk;
    private $salesInAmountForSingara18p50tk;
    private $extSaleForSingara18p50tk;
    private $totalSaleAmountForSingara;
    private $dueReceivedForSingara;
    private $dueInAmountForSingara;
    private $expAndComForSingara;
    private $totalReceivedForSingara;

    // Singara   24p45tk

    private $distributedPacketForSingara12p45tk;
    private $bonusForSingara12p45tk;
    private $replaceForSingara12p45tk;
    private $returnForSingara12p45tk;
    private $totalSalePacketForSingara12p45tk;
    private $salesInAmountForSingara12p45tk;
    private $extSalesForSingara12p45tk;

    //    Chicken Roll  18p95tk

    private $distributedPacketForChickenRoll18p95tk;
    private $bonusForChickenRoll18p95tk;
    private $replaceForChickenRoll18p95tk;
    private $returnForChickenRoll18p95tk;
    private $totalSalePacketForChickenRoll18p95tk;
    private $salesInAmountForChickenRoll18p95tk;
    private $extSaleForChickenRoll18p95tk;
    private $totalSaleAmountForChickenRoll;
    private $dueReceivedForChickenRoll;
    private $dueInAmountForChickenRoll;
    private $expAndComForChickenRoll;
    private $totalReceivedForChickenRoll;

    //Chicken Roll 24p@45

    private $distributedPacketForChickenRoll24p45;
    private $bonusForChickenRoll24p45;
    private $replaceForChickenRoll24p45;
    private $returnForChickenRoll24p45;
    private $totalSalePacketForChickenRoll24p45;
    private $salesInAmountForChickenRoll24p45;
    private $extSalesForChickenRoll24p45;

    //    Veg Roll 18p70tk

    private $distributedPacketForVegRoll18p70tk;
    private $bonusForVegRoll18p70tk;
    private $replaceForVegRoll18p70tk;
    private $returnForVegRoll18p70tk;
    private $totalSalePacketForVegRoll18p70tk;
    private $salesInAmountForVegRoll18p70tk;
    private $extSaleForVegRoll18p70tk;
    private $totalSaleAmountForVegRoll;
    private $dueReceivedForVegRoll;
    private $dueInAmountForVegRoll;
    private $expAndComForVegRoll;
    private $totalReceivedForVegRoll;

    //VegRoll Undefined

    private $distributedPacketForVegRollUndefined;
    private $bonusForVegRollUndefined;
    private $replaceForVegRollUndefined;
    private $returnForVegRollUndefined;
    private $totalSalePacketForVegRollUndefined;
    private $salesInAmountForVegRollUndefined;
    private $extSalesForVegRollUndefined;


    //    Puffed Rice 2kg13tk

    private $distributedPacketForPuffedRice2kg13tk;
    private $bonusForPuffedRice2kg13tk;
    private $replaceForPuffedRice2kg13tk;
    private $returnForPuffedRice2kg13tk;
    private $totalSalePacketForPuffedRice2kg13tk;
    private $salesInAmountForPuffedRice2kg13tk;
    private $extSaleForPuffedRice2kg13tk;
    private $totalSaleAmountForPuffedRice;
    private $dueReceivedForPuffedRice;
    private $dueInAmountForPuffedRice;
    private $expAndComForPuffedRice;
    private $totalReceivedForPuffedRice;

    //Puffed Rice  1kg7tk

    private $distributedPacketForPuffedRice1kg7tk;
    private $bonusForPuffedRice1kg7tk;
    private $replaceForPuffedRice1kg7tk;
    private $returnForPuffedRice1kg7tk;
    private $totalSalePacketForPuffedRice1kg7tk;
    private $salesInAmountForPuffedRice1kg7tk;
    private $extSalesForPuffedRice1kg7tk;


    private $deletedAt;
    private $email;
    private $con;
    private $description;
    private $allbookData = array();
    private $allTrashedBookData = array();

    //TOTAL

    private $distributedPacketTOTAL;
    private $bonusTOTAL;
    private $replaceTOTAL;
    private $returnTOTAL;
    private $totalSalesInPacketTOTAL;
    private $salesInAmountTOTAL;
    private $extSalesTOTAL;
    private $totalSalesInAmount;
    private $dueReceivedTOTAL;
    private $dueAmountTOTAL;
    private $expAndComTOTAL;
    private $totalReceivedTOTAL;

    function __construct()
    {
        $this->con = mysqli_connect('localhost', 'root', "", 'atomicprojectb22');
    }

    public function prepareVariableValue($data)
    {
        // ROTI

        if (array_key_exists('distributedPacketForRoti6p20tk', $data))
            $this->distributedPacketForRoti6p20tk =
                ($data['distributedPacketForRoti6p20tk']);

        if (array_key_exists('bonusForRoti6p20tk', $data))
            $this->bonusForRoti6p20tk =
                ($data['bonusForRoti6p20tk']);

        if (array_key_exists('replaceForRoti6p20tk', $data))
            $this->replaceForRoti6p20tk =
                ($data['replaceForRoti6p20tk']);

        if (array_key_exists('returnForRoti6p20tk', $data))
            $this->returnForRoti6p20tk =
                ($data['returnForRoti6p20tk']);

        if (array_key_exists('totalSalePacketForRoti6p20tk', $data))
            $this->totalSalePacketForRoti6p20tk =
                ($data['totalSalePacketForRoti6p20tk']);

        if (array_key_exists('salesInAmountForRoti6p20tk', $data))
            $this->salesInAmountForRoti6p20tk =
                ($data['salesInAmountForRoti6p20tk']);

        if (array_key_exists('extSaleForRoti6p20tk', $data))
            $this->extSaleForRoti6p20tk =
                ($data['extSaleForRoti6p20tk']);

        if (array_key_exists('totalSaleAmountForRoti', $data))
            $this->totalSaleAmountForRoti =
                ($data['totalSaleAmountForRoti']);

        if (array_key_exists('dueReceivedForRoti', $data))
            $this->dueReceivedForRoti =
                ($data['dueReceivedForRoti']);

        if (array_key_exists('dueInAmountForRoti', $data))
            $this->dueInAmountForRoti =
                ($data['dueInAmountForRoti']);

        if (array_key_exists('expAndComForRoti', $data))
            $this->expAndComForRoti =
                ($data['expAndComForRoti']);

        if (array_key_exists('totalReceivedForRoti', $data))
            $this->totalReceivedForRoti =
                ($data['totalReceivedForRoti']);

        if (array_key_exists('distributedPacketForRoti4p14tk', $data))
            $this->distributedPacketForRoti4p14tk =
                ($data['distributedPacketForRoti4p14tk']);

        if (array_key_exists('bonusForRoti4p14tk', $data))
            $this->bonusForRoti4p14tk =
                ($data['bonusForRoti4p14tk']);

        if (array_key_exists('replaceForRoti4p14tk', $data))
            $this->replaceForRoti4p14tk =
                ($data['replaceForRoti4p14tk']);

        if (array_key_exists('returnForRoti4p14tk', $data))
            $this->returnForRoti4p14tk =
                ($data['returnForRoti4p14tk']);

        if (array_key_exists('totalSalePacketForRoti4p14tk', $data))
            $this->totalSalePacketForRoti4p14tk =
                ($data['totalSalePacketForRoti4p14tk']);

        if (array_key_exists('salesInAmountForRoti4p14tk', $data))
            $this->salesInAmountForRoti4p14tk =
                ($data['salesInAmountForRoti4p14tk']);

        if (array_key_exists('extSalesForRoti4p14tk', $data))
            $this->extSalesForRoti4p14tk =
                ($data['extSalesForRoti4p14tk']);

        // DONUT

        if (array_key_exists('distributedPacketForDonut2p18tk', $data))
            $this->distributedPacketForDonut2p18tk =
                ($data['distributedPacketForDonut2p18tk']);

        if (array_key_exists('bonusForDonut2p18tk', $data))
            $this->bonusForDonut2p18tk =
                ($data['bonusForDonut2p18tk']);

        if (array_key_exists('replaceForDonut2p18tk', $data))
            $this->replaceForDonut2p18tk =
                ($data['replaceForDonut2p18tk']);

        if (array_key_exists('returnForDonut2p18tk', $data))
            $this->returnForDonut2p18tk =
                ($data['returnForDonut2p18tk']);

        if (array_key_exists('totalSalePacketForDonut2p18tk', $data))
            $this->totalSalePacketForDonut2p18tk =
                ($data['totalSalePacketForDonut2p18tk']);

        if (array_key_exists('salesInAmountForDonut2p18tk', $data))
            $this->salesInAmountForDonut2p18tk =
                ($data['salesInAmountForDonut2p18tk']);

        if (array_key_exists('extSaleForDonut2p18tk', $data))
            $this->extSaleForDonut2p18tk =
                ($data['extSaleForDonut2p18tk']);

        if (array_key_exists('totalSaleAmountForDonut', $data))
            $this->totalSaleAmountForDonut =
                ($data['totalSaleAmountForDonut']);


        if (array_key_exists('dueReceivedForDonut', $data))
            $this->dueReceivedForDonut =
                ($data['dueReceivedForDonut']);

        if (array_key_exists('dueInAmountForDonut', $data))
            $this->dueInAmountForDonut =
                ($data['dueInAmountForDonut']);

        if (array_key_exists('expAndComForDonut', $data))
            $this->expAndComForDonut =
                ($data['expAndComForDonut']);

        if (array_key_exists('totalReceivedForDonut', $data))
            $this->totalReceivedForDonut =
                ($data['totalReceivedForDonut']);

        if (array_key_exists('distributedPacketForDonut3p22tk', $data))
            $this->distributedPacketForDonut3p22tk =
                ($data['distributedPacketForDonut3p22tk']);

        if (array_key_exists('bonusForDonut3p22tk', $data))
            $this->bonusForDonut3p22tk =
                ($data['bonusForDonut3p22tk']);

        if (array_key_exists('replaceForDonut3p22tk', $data))
            $this->replaceForDonut3p22tk =
                ($data['replaceForDonut3p22tk']);

        if (array_key_exists('returnForDonut3p22tk', $data))
            $this->returnForDonut3p22tk =
                ($data['returnForDonut3p22tk']);

        if (array_key_exists('totalSalePacketForDonut3p22tk', $data))
            $this->totalSalePacketForDonut3p22tk =
                ($data['totalSalePacketForDonut3p22tk']);

        if (array_key_exists('salesInAmountForDonut3p22tk', $data))
            $this->salesInAmountForDonut3p22tk =
                ($data['salesInAmountForDonut3p22tk']);

        if (array_key_exists('extSalesForDonut3p22tk', $data))
            $this->extSalesForDonut3p22tk =
                ($data['extSalesForDonut3p22tk']);

        // SOMOSA (BEEF)

        if (array_key_exists('distributedPacketForSamosaBeef24p45tk', $data))
            $this->distributedPacketForSamosaBeef24p45tk =
                ($data['distributedPacketForSamosaBeef24p45tk']);

        if (array_key_exists('bonusForSamosaBeef24p45tk', $data))
            $this->bonusForSamosaBeef24p45tk =
                ($data['bonusForSamosaBeef24p45tk']);

        if (array_key_exists('replaceForSamosaBeef24p45tk', $data))
            $this->replaceForSamosaBeef24p45tk =
                ($data['replaceForSamosaBeef24p45tk']);

        if (array_key_exists('returnForSamosaBeef24p45tk', $data))
            $this->returnForSamosaBeef24p45tk =
                ($data['returnForSamosaBeef24p45tk']);

        if (array_key_exists('totalSalePacketForSamosaBeef24p45tk', $data))
            $this->totalSalePacketForSamosaBeef24p45tk =
                ($data['totalSalePacketForSamosaBeef24p45tk']);

        if (array_key_exists('salesInAmountForSamosaBeef24p45tk', $data))
            $this->salesInAmountForSamosaBeef24p45tk =
                ($data['salesInAmountForSamosaBeef24p45tk']);

        if (array_key_exists('extSaleForSamosaBeef24p45tk', $data))
            $this->extSaleForSamosaBeef24p45tk =
                ($data['extSaleForSamosaBeef24p45tk']);

        if (array_key_exists('totalSaleAmountForSamosaBeef', $data))
            $this->totalSaleAmountForSamosaBeef =
                ($data['totalSaleAmountForSamosaBeef']);


        if (array_key_exists('dueReceivedForSamosaBeef', $data))
            $this->dueReceivedForSamosaBeef =
                ($data['dueReceivedForSamosaBeef']);

        if (array_key_exists('dueInAmountForSamosaBeef', $data))
            $this->dueInAmountForSamosaBeef =
                ($data['dueInAmountForSamosaBeef']);

        if (array_key_exists('expAndComForSamosaBeef', $data))
            $this->expAndComForSamosaBeef =
                ($data['expAndComForSamosaBeef']);

        if (array_key_exists('totalReceivedForSamosaBeef', $data))
            $this->totalReceivedForSamosaBeef =
                ($data['totalReceivedForSamosaBeef']);

        if (array_key_exists('distributedPacketForSamosaBeef12p45tk', $data))
            $this->distributedPacketForSamosaBeef12p45tk =
                ($data['distributedPacketForSamosaBeef12p45tk']);

        if (array_key_exists('bonusForSamosaBeef12p45tk', $data))
            $this->bonusForSamosaBeef12p45tk =
                ($data['bonusForSamosaBeef12p45tk']);

        if (array_key_exists('replaceForSamosaBeef12p45tk', $data))
            $this->replaceForSamosaBeef12p45tk =
                ($data['replaceForSamosaBeef12p45tk']);

        if (array_key_exists('returnForSamosaBeef12p45tk', $data))
            $this->returnForSamosaBeef12p45tk =
                ($data['returnForSamosaBeef12p45tk']);

        if (array_key_exists('totalSalePacketForSamosaBeef12p45tk', $data))
            $this->totalSalePacketForSamosaBeef12p45tk =
                ($data['totalSalePacketForSamosaBeef12p45tk']);

        if (array_key_exists('salesInAmountForSamosaBeef12p45tk', $data))
            $this->salesInAmountForSamosaBeef12p45tk =
                ($data['salesInAmountForSamosaBeef12p45tk']);

        if (array_key_exists('extSalesForSamosaBeef12p45tk', $data))
            $this->extSalesForSamosaBeef12p45tk =
                ($data['extSalesForSamosaBeef12p45tk']);

        // SOMOSA (CHICKEN)

        if (array_key_exists('distributedPacketForSamosaChicken24p80tk', $data))
            $this->distributedPacketForSamosaChicken24p80tk =
                ($data['distributedPacketForSamosaChicken24p80tk']);

        if (array_key_exists('bonusForSamosaChicken24p80tk', $data))
            $this->bonusForSamosaChicken24p80tk =
                ($data['bonusForSamosaChicken24p80tk']);

        if (array_key_exists('replaceForSamosaChicken24p80tk', $data))
            $this->replaceForSamosaChicken24p80tk =
                ($data['replaceForSamosaChicken24p80tk']);

        if (array_key_exists('returnForSamosaChicken24p80tk', $data))
            $this->returnForSamosaChicken24p80tk =
                ($data['returnForSamosaChicken24p80tk']);

        if (array_key_exists('totalSalePacketForSamosaChicken24p80tk', $data))
            $this->totalSalePacketForSamosaChicken24p80tk =
                ($data['totalSalePacketForSamosaChicken24p80tk']);

        if (array_key_exists('salesInAmountForSamosaChicken24p80tk', $data))
            $this->salesInAmountForSamosaChicken24p80tk =
                ($data['salesInAmountForSamosaChicken24p80tk']);

        if (array_key_exists('extSaleForSamosaChicken24p80tk', $data))
            $this->extSaleForSamosaChicken24p80tk =
                ($data['extSaleForSamosaChicken24p80tk']);

        if (array_key_exists('totalSaleAmountForSamosaChicken', $data))
            $this->totalSaleAmountForSamosaChicken =
                ($data['totalSaleAmountForSamosaChicken']);


        if (array_key_exists('dueReceivedForSamosaChicken', $data))
            $this->dueReceivedForSamosaChicken =
                ($data['dueReceivedForSamosaChicken']);

        if (array_key_exists('dueInAmountForSamosaChicken', $data))
            $this->dueInAmountForSamosaChicken =
                ($data['dueInAmountForSamosaChicken']);

        if (array_key_exists('expAndComForSamosaChicken', $data))
            $this->expAndComForSamosaChicken =
                ($data['expAndComForSamosaChicken']);

        if (array_key_exists('totalReceivedForSamosaChicken', $data))
            $this->totalReceivedForSamosaChicken =
                ($data['totalReceivedForSamosaChicken']);

        if (array_key_exists('distributedPacketForSamosaChicken12p45', $data))
            $this->distributedPacketForSamosaChicken12p45 =
                ($data['distributedPacketForSamosaChicken12p45']);

        if (array_key_exists('bonusForSamosaChicken12p45', $data))
            $this->bonusForSamosaChicken12p45 =
                ($data['bonusForSamosaChicken12p45']);

        if (array_key_exists('replaceForSamosaChicken12p45', $data))
            $this->replaceForSamosaChicken12p45 =
                ($data['replaceForSamosaChicken12p45']);

        if (array_key_exists('returnForSamosaChicken12p45', $data))
            $this->returnForSamosaChicken12p45 =
                ($data['returnForSamosaChicken12p45']);

        if (array_key_exists('totalSalePacketForSamosaChicken12p45', $data))
            $this->totalSalePacketForSamosaChicken12p45 =
                ($data['totalSalePacketForSamosaChicken12p45']);

        if (array_key_exists('salesInAmountForSamosaChicken12p45', $data))
            $this->salesInAmountForSamosaChicken12p45 =
                ($data['salesInAmountForSamosaChicken12p45']);

        if (array_key_exists('extSalesForSamosaChicken12p45', $data))
            $this->extSalesForSamosaChicken12p45 =
                ($data['extSalesForSamosaChicken12p45']);


        //SIGARA

        if (array_key_exists('distributedPacketForSingara18p50tk', $data))
            $this->distributedPacketForSingara18p50tk =
                ($data['distributedPacketForSingara18p50tk']);

        if (array_key_exists('bonusForSingara18p50tk', $data))
            $this->bonusForSingara18p50tk =
                ($data['bonusForSingara18p50tk']);

        if (array_key_exists('replaceForSingara18p50tk', $data))
            $this->replaceForSingara18p50tk =
                ($data['replaceForSingara18p50tk']);

        if (array_key_exists('returnForSingara18p50tk', $data))
            $this->returnForSingara18p50tk =
                ($data['returnForSingara18p50tk']);

        if (array_key_exists('totalSalePacketForSingara18p50tk', $data))
            $this->totalSalePacketForSingara18p50tk =
                ($data['totalSalePacketForSingara18p50tk']);

        if (array_key_exists('salesInAmountForSingara18p50tk', $data))
            $this->salesInAmountForSingara18p50tk =
                ($data['salesInAmountForSingara18p50tk']);

        if (array_key_exists('extSaleForSingara18p50tk', $data))
            $this->extSaleForSingara18p50tk =
                ($data['extSaleForSingara18p50tk']);

        if (array_key_exists('totalSaleAmountForSingara', $data))
            $this->totalSaleAmountForSingara =
                ($data['totalSaleAmountForSingara']);


        if (array_key_exists('dueReceivedForSingara', $data))
            $this->dueReceivedForSingara =
                ($data['dueReceivedForSingara']);

        if (array_key_exists('dueInAmountForSingara', $data))
            $this->dueInAmountForSingara =
                ($data['dueInAmountForSingara']);

        if (array_key_exists('expAndComForSingara', $data))
            $this->expAndComForSingara =
                ($data['expAndComForSingara']);

        if (array_key_exists('totalReceivedForSingara', $data))
            $this->totalReceivedForSingara =
                ($data['totalReceivedForSingara']);

        if (array_key_exists('distributedPacketForSingara12p45tk', $data))
            $this->distributedPacketForSingara12p45tk =
                ($data['distributedPacketForSingara12p45tk']);

        if (array_key_exists('bonusForSingara12p45tk', $data))
            $this->bonusForSingara12p45tk =
                ($data['bonusForSingara12p45tk']);

        if (array_key_exists('replaceForSingara12p45tk', $data))
            $this->replaceForSingara12p45tk =
                ($data['replaceForSingara12p45tk']);

        if (array_key_exists('returnForSingara12p45tk', $data))
            $this->returnForSingara12p45tk =
                ($data['returnForSingara12p45tk']);

        if (array_key_exists('totalSalePacketForSingara12p45tk', $data))
            $this->totalSalePacketForSingara12p45tk =
                ($data['totalSalePacketForSingara12p45tk']);

        if (array_key_exists('salesInAmountForSingara12p45tk', $data))
            $this->salesInAmountForSingara12p45tk =
                ($data['salesInAmountForSingara12p45tk']);

        if (array_key_exists('extSalesForSingara12p45tk', $data))
            $this->extSalesForSingara12p45tk =
                ($data['extSalesForSingara12p45tk']);

        //CHICKEN SPRING ROLL

        if (array_key_exists('distributedPacketForChickenRoll18p95tk', $data))
            $this->distributedPacketForChickenRoll18p95tk =
                ($data['distributedPacketForChickenRoll18p95tk']);

        if (array_key_exists('bonusForChickenRoll18p95tk', $data))
            $this->bonusForChickenRoll18p95tk =
                ($data['bonusForChickenRoll18p95tk']);

        if (array_key_exists('replaceForChickenRoll18p95tk', $data))
            $this->replaceForChickenRoll18p95tk =
                ($data['replaceForChickenRoll18p95tk']);

        if (array_key_exists('returnForChickenRoll18p95tk', $data))
            $this->returnForChickenRoll18p95tk =
                ($data['returnForChickenRoll18p95tk']);

        if (array_key_exists('totalSalePacketForChickenRoll18p95tk', $data))
            $this->totalSalePacketForChickenRoll18p95tk =
                ($data['totalSalePacketForChickenRoll18p95tk']);

        if (array_key_exists('salesInAmountForChickenRoll18p95tk', $data))
            $this->salesInAmountForChickenRoll18p95tk =
                ($data['salesInAmountForChickenRoll18p95tk']);

        if (array_key_exists('extSaleForChickenRoll18p95tk', $data))
            $this->extSaleForChickenRoll18p95tk =
                ($data['extSaleForChickenRoll18p95tk']);

        if (array_key_exists('totalSaleAmountForChickenRoll', $data))
            $this->totalSaleAmountForChickenRoll =
                ($data['totalSaleAmountForChickenRoll']);


        if (array_key_exists('dueReceivedForChickenRoll', $data))
            $this->dueReceivedForChickenRoll =
                ($data['dueReceivedForChickenRoll']);

        if (array_key_exists('dueInAmountForChickenRoll', $data))
            $this->dueInAmountForChickenRoll =
                ($data['dueInAmountForChickenRoll']);

        if (array_key_exists('expAndComForChickenRoll', $data))
            $this->expAndComForChickenRoll =
                ($data['expAndComForChickenRoll']);

        if (array_key_exists('totalReceivedForChickenRoll', $data))
            $this->totalReceivedForChickenRoll =
                ($data['totalReceivedForChickenRoll']);

        if (array_key_exists('distributedPacketForChickenRoll24p45', $data))
            $this->distributedPacketForChickenRoll24p45 =
                ($data['distributedPacketForChickenRoll24p45']);

        if (array_key_exists('bonusForChickenRoll24p45', $data))
            $this->bonusForChickenRoll24p45 =
                ($data['bonusForChickenRoll24p45']);

        if (array_key_exists('replaceForChickenRoll24p45', $data))
            $this->replaceForChickenRoll24p45 =
                ($data['replaceForChickenRoll24p45']);

        if (array_key_exists('returnForChickenRoll24p45', $data))
            $this->returnForChickenRoll24p45 =
                ($data['returnForChickenRoll24p45']);

        if (array_key_exists('totalSalePacketForChickenRoll24p45', $data))
            $this->totalSalePacketForChickenRoll24p45 =
                ($data['totalSalePacketForChickenRoll24p45']);

        if (array_key_exists('salesInAmountForChickenRoll24p45', $data))
            $this->salesInAmountForChickenRoll24p45 =
                ($data['salesInAmountForChickenRoll24p45']);

        if (array_key_exists('extSalesForChickenRoll24p45', $data))
            $this->extSalesForChickenRoll24p45 =
                ($data['extSalesForChickenRoll24p45']);


        //VEGETABLE SPRING ROLL

        if (array_key_exists('distributedPacketForVegRoll18p70tk', $data))
            $this->distributedPacketForVegRoll18p70tk =
                ($data['distributedPacketForVegRoll18p70tk']);

        if (array_key_exists('bonusForVegRoll18p70tk', $data))
            $this->bonusForVegRoll18p70tk =
                ($data['bonusForVegRoll18p70tk']);

        if (array_key_exists('replaceForVegRoll18p70tk', $data))
            $this->replaceForVegRoll18p70tk =
                ($data['replaceForVegRoll18p70tk']);

        if (array_key_exists('returnForVegRoll18p70tk', $data))
            $this->returnForVegRoll18p70tk =
                ($data['returnForVegRoll18p70tk']);

        if (array_key_exists('totalSalePacketForVegRoll18p70tk', $data))
            $this->totalSalePacketForVegRoll18p70tk =
                ($data['totalSalePacketForVegRoll18p70tk']);

        if (array_key_exists('salesInAmountForVegRoll18p70tk', $data))
            $this->salesInAmountForVegRoll18p70tk =
                ($data['salesInAmountForVegRoll18p70tk']);

        if (array_key_exists('extSaleForVegRoll18p70tk', $data))
            $this->extSaleForVegRoll18p70tk =
                ($data['extSaleForVegRoll18p70tk']);

        if (array_key_exists('totalSaleAmountForVegRoll', $data))
            $this->totalSaleAmountForVegRoll =
                ($data['totalSaleAmountForVegRoll']);


        if (array_key_exists('dueReceivedForVegRoll', $data))
            $this->dueReceivedForVegRoll =
                ($data['dueReceivedForVegRoll']);

        if (array_key_exists('dueInAmountForVegRoll', $data))
            $this->dueInAmountForVegRoll =
                ($data['dueInAmountForVegRoll']);

        if (array_key_exists('expAndComForVegRoll', $data))
            $this->expAndComForVegRoll =
                ($data['expAndComForVegRoll']);

        if (array_key_exists('totalReceivedForVegRoll', $data))
            $this->totalReceivedForVegRoll =
                ($data['totalReceivedForVegRoll']);

        if (array_key_exists('distributedPacketForVegRollUndefined', $data))
            $this->distributedPacketForVegRollUndefined =
                ($data['distributedPacketForVegRollUndefined']);

        if (array_key_exists('bonusForVegRollUndefined', $data))
            $this->bonusForVegRollUndefined =
                ($data['bonusForVegRollUndefined']);

        if (array_key_exists('replaceForVegRollUndefined', $data))
            $this->replaceForVegRollUndefined =
                ($data['replaceForVegRollUndefined']);

        if (array_key_exists('returnForVegRollUndefined', $data))
            $this->returnForVegRollUndefined =
                ($data['returnForVegRollUndefined']);

        if (array_key_exists('totalSalePacketForVegRollUndefined', $data))
            $this->totalSalePacketForVegRollUndefined =
                ($data['totalSalePacketForVegRollUndefined']);

        if (array_key_exists('salesInAmountForVegRollUndefined', $data))
            $this->salesInAmountForVegRollUndefined =
                ($data['salesInAmountForVegRollUndefined']);

        if (array_key_exists('extSalesForVegRollUndefined', $data))
            $this->extSalesForVegRollUndefined =
                ($data['extSalesForVegRollUndefined']);

        //PUFFED RICE

        if (array_key_exists('distributedPacketForPuffedRice2kg13tk', $data))
            $this->distributedPacketForPuffedRice2kg13tk =
                ($data['distributedPacketForPuffedRice2kg13tk']);

        if (array_key_exists('bonusForPuffedRice2kg13tk', $data))
            $this->bonusForPuffedRice2kg13tk =
                ($data['bonusForPuffedRice2kg13tk']);

        if (array_key_exists('replaceForPuffedRice2kg13tk', $data))
            $this->replaceForPuffedRice2kg13tk =
                ($data['replaceForPuffedRice2kg13tk']);

        if (array_key_exists('returnForPuffedRice2kg13tk', $data))
            $this->returnForPuffedRice2kg13tk =
                ($data['returnForPuffedRice2kg13tk']);

        if (array_key_exists('totalSalePacketForPuffedRice2kg13tk', $data))
            $this->totalSalePacketForPuffedRice2kg13tk =
                ($data['totalSalePacketForPuffedRice2kg13tk']);

        if (array_key_exists('salesInAmountForPuffedRice2kg13tk', $data))
            $this->salesInAmountForPuffedRice2kg13tk =
                ($data['salesInAmountForPuffedRice2kg13tk']);

        if (array_key_exists('extSaleForPuffedRice2kg13tk', $data))
            $this->extSaleForPuffedRice2kg13tk =
                ($data['extSaleForPuffedRice2kg13tk']);

        if (array_key_exists('totalSaleAmountForPuffedRice', $data))
            $this->totalSaleAmountForPuffedRice =
                ($data['totalSaleAmountForPuffedRice']);


        if (array_key_exists('dueReceivedForPuffedRice', $data))
            $this->dueReceivedForPuffedRice =
                ($data['dueReceivedForPuffedRice']);

        if (array_key_exists('dueInAmountForPuffedRice', $data))
            $this->dueInAmountForPuffedRice =
                ($data['dueInAmountForPuffedRice']);

        if (array_key_exists('expAndComForPuffedRice', $data))
            $this->expAndComForPuffedRice =
                ($data['expAndComForPuffedRice']);

        if (array_key_exists('totalReceivedForPuffedRice', $data))
            $this->totalReceivedForPuffedRice =
                ($data['totalReceivedForPuffedRice']);

        if (array_key_exists('distributedPacketForPuffedRice1kg7tk', $data))
            $this->distributedPacketForPuffedRice1kg7tk =
                ($data['distributedPacketForPuffedRice1kg7tk']);

        if (array_key_exists('bonusForPuffedRice1kg7tk', $data))
            $this->bonusForPuffedRice1kg7tk =
                ($data['bonusForPuffedRice1kg7tk']);

        if (array_key_exists('replaceForPuffedRice1kg7tk', $data))
            $this->replaceForPuffedRice1kg7tk =
                ($data['replaceForPuffedRice1kg7tk']);

        if (array_key_exists('returnForPuffedRice1kg7tk', $data))
            $this->returnForPuffedRice1kg7tk =
                ($data['returnForPuffedRice1kg7tk']);

        if (array_key_exists('totalSalePacketForPuffedRice1kg7tk', $data))
            $this->totalSalePacketForPuffedRice1kg7tk =
                ($data['totalSalePacketForPuffedRice1kg7tk']);

        if (array_key_exists('salesInAmountForPuffedRice1kg7tk', $data))
            $this->salesInAmountForPuffedRice1kg7tk =
                ($data['salesInAmountForPuffedRice1kg7tk']);

        if (array_key_exists('extSalesForPuffedRice1kg7tk', $data))
            $this->extSalesForPuffedRice1kg7tk =
                ($data['extSalesForPuffedRice1kg7tk']);

        // TOTAL
        if (array_key_exists('distributedPacketTOTAL', $data))
            $this->distributedPacketTOTAL =
                ($data['distributedPacketTOTAL']);

        if (array_key_exists('bonusTOTAL', $data))
            $this->bonusTOTAL =
                ($data['bonusTOTAL']);

        if (array_key_exists('replaceTOTAL', $data))
            $this->replaceTOTAL =
                ($data['replaceTOTAL']);

        if (array_key_exists('returnTOTAL', $data))
            $this->returnTOTAL =
                ($data['returnTOTAL']);

        if (array_key_exists('totalSalesInPacketTOTAL', $data))
            $this->totalSalesInPacketTOTAL =
                ($data['totalSalesInPacketTOTAL']);

        if (array_key_exists('salesInAmountTOTAL', $data))
            $this->salesInAmountTOTAL =
                ($data['salesInAmountTOTAL']);

        if (array_key_exists('extSalesTOTAL', $data))
            $this->extSalesTOTAL =
                ($data['extSalesTOTAL']);

        if (array_key_exists('totalSalesInAmount', $data))
            $this->totalSalesInAmount =
                ($data['totalSalesInAmount']);

        if (array_key_exists('dueReceivedTOTAL', $data))
            $this->dueReceivedTOTAL =
                ($data['dueReceivedTOTAL']);
        
        if (array_key_exists('dueAmountTOTAL', $data))
            $this->dueAmountTOTAL =
                ($data['dueAmountTOTAL']);

        if (array_key_exists('expAndComTOTAL', $data))
            $this->expAndComTOTAL =
                ($data['expAndComTOTAL']);
        
        if (array_key_exists('totalReceivedTOTAL', $data))
            $this->totalReceivedTOTAL =
                ($data['totalReceivedTOTAL']);

    }

    function store()
    {
        $result = mysqli_query($this->con, "INSERT INTO 
`atomicprojectb22`.`book` ( `title`, `description`, `email`)
VALUES ('" . $this->title . "', '" . $this->description . "', '" . $this->email . "')");


        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been stored successfully!</strong>
            </div>");
        } else Message::examineMessage("error");

        //echo $result;
        // echo $this->title;

        //return " I am storing data ";
    }

    function update()
    {
        $query = "UPDATE `book` SET `title` = '" . $this->title . "' 
        WHERE `book`.`id` = " . $this->id;
        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been Updated successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`book` WHERE `book`.`id` =" . $this->id;

        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been deleted successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function view()
    {
        $query = "SELECT * FROM `book` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    function trash()
    {
        $this->deletedAt = time();

        $sql = "UPDATE `book` SET `deleted_at` = '" . $this->deletedAt . "' WHERE `book`.`id` =" . $this->id;
        $result = mysqli_query($this->con, $sql);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been trashed successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function trashed()
    {
        $sql = "SELECT * FROM `book` where deleted_at is not null";
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in allBookData variable and return it to show the data
        // in index.php file
// we must declare allBookData as array to contain all data
        while ($row = mysqli_fetch_object($result))
            $this->allTrashedBookData[] = $row;

        return $this->allTrashedBookData;
    }

    function recover()
    {
        $sql = "update book set deleted_at = null where id = " . $this->id;
        $result = mysqli_query($this->con, $sql);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been recovered successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function recoverSelected($ids = array())
    {
        if (is_array($ids) && count($ids) > 0) {
            $IDs = implode(",", $ids);

            $sql = "update book set deleted_at = NULL WHERE id IN (" . $IDs . ")";
            $result = mysqli_query($this->con, $sql);

            if ($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been recovered successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    function deleteSelected($IDs = array())
    {
        if (is_array($IDs) && count($IDs) > 0) {
            $ids = implode(",", $IDs);

            $sql = 'delete from `book` where id IN (' . $ids . ')';

            $result = mysqli_query($this->con, $sql);

            if ($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been Deleted successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    public function count()
    {
        $query = "SELECT COUNT(*) AS totalItem FROM `book` WHERE deleted_at IS NULL";
        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }

    public function paginator($pageStartFrom = 0, $Limit = 5)
    {
        $_allBook = array();
        $query = "SELECT * FROM `book` WHERE deleted_at is null LIMIT " . $pageStartFrom . "," . $Limit;
        $result = mysqli_query($this->con, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }
}