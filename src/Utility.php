<?php

namespace App\App\Utility;


class Utility
{
    // d for debug
    public static function d($data) {
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }


    // dd for debug and die
    public static function dd($data) {
        echo "<pre>";
        var_dump($data);
        echo "</pre>";

        die();
    }

    public static function reDirectAPageIntoAnotherPage ($data) {
        header("Location: ".$data);
    }

}