<?php
/**
 * Created by PhpStorm.
 * User: Shaon
 * Date: 10/21/2016
 * Time: 2:50 PM
 */

include_once('../vendor/autoload.php');

use App\Tortilla;

$tortilla = new Tortilla;
$tortilla->prepareVariableValue($_POST);
$tortilla->store();
