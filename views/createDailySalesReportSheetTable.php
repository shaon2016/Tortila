<!DOCTYPE html>
<head>
    <title>Tortilla Daily Sell 1</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--    for offline -->
    <link rel="stylesheet" type="text/css" href="../../../resource/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resource/js/bootstrap.js">
    <!--for online also works on ofline-->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>

<body>

<div>
    <form class="form-horizontal" role="form" action="" method="post">
        <table border="1">
            <tr>
                <th>Product Title</th>
                <th>PIECE and RATE</th>
                <td>DISTRIB. PACKET</td>
                <td>(BONUS) (PACKET)</td>
                <td>(REPLACE) (PACKET)</td>
                <td>(RETURN)(PACKET)</td>
                <td>TOTAL SALE (PACKET)</td>
                <td>SALES IN AMOUNT</td>
                <td>EXT. SALE (PACKET)</td>
                <td>TOTAL SALE (AMOUNT)</td>
                <td>DUE RECEIVED</td>
                <td>(DUE IN AMOUNT)</td>
                <td>(EXP. & COM.)</td>
                <td>TOTAL RECEIVED</td>
            </tr>
            <!--           ROTI  -->
            <tr>
                <th rowspan="2">ROTI</th>
                <td>6p@20</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForRoti6p20tk"
                           name='distributedPacketForRoti6p20tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForRoti6p20tk" name='bonusForRoti6p20tk'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForRoti6p20tk" name='replaceForRoti6p20tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForRoti6p20tk" name='returnForRoti6p20tk'
                           value="0">
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForRoti6p20tk" name='totalSalePacketForRoti6p20tk'
                           value="0" readonly>

                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForRoti6p20tk" name='salesInAmountForRoti6p20tk'
                           value="0" readonly>
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSaleForRoti6p20tk" name='extSaleForRoti6p20tk'
                           value="0">
                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalSaleAmountForRoti" name='totalSaleAmountForRoti'
                           value="0" readonly>
                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueReceivedForRoti" name='dueReceivedForRoti'
                           value="0">

                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueInAmountForRoti" name='dueInAmountForRoti'
                           value="0">

                </td>
                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="expAndComForRoti" name='expAndComForRoti'
                           value="0">

                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalReceivedForRoti" name='totalReceivedForRoti'
                           value="0" readonly>

                </td>

            </tr>


            <!--           ROTI  -->
            <tr>
                <td>4p@14</td>
                <td>
                    <input type="text"
                           class="form-control" name='distributedPacketForRoti4p14tk'
                           id="distributedPacketForRoti4p14tk" value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" name='bonusForRoti4p14tk'
                           id="bonusForRoti4p14tk" value="0">
                </td>
                <td><input type="text"
                           class="form-control" name='replaceForRoti4p14tk'
                           id="replaceForRoti4p14tk" value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" name='returnForRoti4p14tk'
                           id="returnForRoti4p14tk" value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" name='totalSalePacketForRoti4p14tk'
                           id="totalSalePacketForRoti4p14tk" value="0" readonly>

                </td>

                <td>
                    <input type="text"
                           class="form-control" name='salesInAmountForRoti4p14tk'
                           id="salesInAmountForRoti4p14tk" value="0" readonly>

                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSalesForRoti4p14tk"
                           name='extSalesForRoti4p14tk'
                           value="0">
                </td>


            </tr>
            <!--**********************************************************************-->

            <!--           Donut  -->
            <tr>
                <th rowspan="2">DONUT</th>
                <td>2p@18</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForDonut2p18tk"
                           name='distributedPacketForDonut2p18tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForDonut2p18tk" name='bonusForDonut2p18tk'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForDonut2p18tk" name='replaceForDonut2p18tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForDonut2p18tk" name='returnForDonut2p18tk'
                           value="0">
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForDonut2p18tk" name='totalSalePacketForDonut2p18tk'
                           value="0" readonly>
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForDonut2p18tk" name='salesInAmountForDonut2p18tk'
                           value="0" readonly>
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSaleForDonut2p18tk" name='extSaleForDonut2p18tk'
                           value="0">
                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalSaleAmountForDonut" name='totalSaleAmountForDonut'
                           value="0" readonly>

                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueReceivedForDonut" name='dueReceivedForDonut'
                           value="0">

                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueInAmountForDonut" name='dueInAmountForDonut'
                           value="0">

                </td>
                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="expAndComForDonut" name='expAndComForDonut'
                           value="0">

                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalReceivedForDonut" name='totalReceivedForDonut'
                           value="0" readonly>
                </td>
            </tr>

            <tr>
                <td>3p@22</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForDonut3p22tk"
                           name='distributedPacketForDonut3p22tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForDonut3p22tk" name='bonusForDonut3p22tk'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForDonut3p22tk" name='replaceForDonut3p22tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForDonut3p22tk" name='returnForDonut3p22tk'
                           value="0">
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForDonut3p22tk" name='totalSalePacketForDonut3p22tk'
                           value="0" readonly>
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForDonut3p22tk" name='salesInAmountForDonut3p22tk'
                           value="0" readonly>

                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSalesForDonut3p22tk" name='extSalesForDonut3p22tk'
                           value="0">
                </td>

            </tr>
            <!--**********************SOMOSA BEEF*************************************************-->
            <tr>
                <th rowspan="2">SAMOSA (BEEF)</th>
                <td>24p@45...</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForSamosaBeef24p45tk"
                           name='distributedPacketForSamosaBeef24p45tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForSamosaBeef24p45tk" name='bonusForSamosaBeef24p45tk'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForSamosaBeef24p45tk" name='replaceForSamosaBeef24p45tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForSamosaBeef24p45tk" name='returnForSamosaBeef24p45tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForSamosaBeef24p45tk"
                           name='totalSalePacketForSamosaBeef24p45tk'
                           value="0" readonly>

                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForSamosaBeef24p45tk"
                           name='salesInAmountForSamosaBeef24p45tk'
                           value="0" readonly>

                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSaleForSamosaBeef24p45tk" name='extSaleForSamosaBeef24p45tk'
                           value="0">
                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalSaleAmountForSamosaBeef" name='totalSaleAmountForSamosaBeef'
                           value="0" readonly>

                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueReceivedForSamosaBeef" name='dueReceivedForSamosaBeef'
                           value="0">

                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueInAmountForSamosaBeef" name='dueInAmountForSamosaBeef'
                           value="0">

                </td>
                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="expAndComForSamosaBeef" name='expAndComForSamosaBeef'
                           value="0">

                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalReceivedForSamosaBeef" name='totalReceivedForSamosaBeef'
                           value="0" readonly>

                </td>
            </tr>


            <tr>
                <td>12@45tk...</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForSamosaBeef12p45tk"
                           name='distributedPacketForSamosaBeef12p45tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForSamosaBeef12p45tk" name='bonusForSamosaBeef12p45tk'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForSamosaBeef12p45tk" name='replaceForSamosaBeef12p45tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForSamosaBeef12p45tk" name='returnForSamosaBeef12p45tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForSamosaBeef12p45tk"
                           name='totalSalePacketForSamosaBeef12p45tk'
                           value="0" readonly>
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForSamosaBeef12p45tk"
                           name='salesInAmountForSamosaBeef12p45tk'
                           value="0" readonly>
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSalesForSamosaBeef12p45tk" name='extSalesForSamosaBeef12p45tk'
                           value="0">
                </td>
            </tr>

            <!--*************************SOMOSA CHICKEN******************************************-->
            <tr>
                <th rowspan="2">SAMOSA (CHICKEN)</th>
                <td>24p@80</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForSamosaChicken24p80tk"
                           name='distributedPacketForSamosaChicken24p80tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForSamosaChicken24p80tk" name='bonusForSamosaChicken24p80tk'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForSamosaChicken24p80tk"
                           name='replaceForSamosaChicken24p80tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForSamosaChicken24p80tk" name='returnForSamosaChicken24p80tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForSamosaChicken24p80tk"
                           name='totalSalePacketForSamosaChicken24p80tk'
                           value="0" readonly>
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForSamosaChicken24p80tk"
                           name='salesInAmountForSamosaChicken24p80tk'
                           value="0" readonly>

                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSaleForSamosaChicken24p80tk"
                           name='extSaleForSamosaChicken24p80tk'
                           value="0">
                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalSaleAmountForSamosaChicken"
                           id="totalSaleAmountForSamosaChicken"
                           name='totalSaleAmountForSamosaChicken'
                           value="0" readonly>

                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueReceivedForSamosaChicken" name='dueReceivedForSamosaChicken'
                           value="0">

                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueInAmountForSamosaChicken" name='dueInAmountForSamosaChicken'
                           value="0">

                </td>
                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="expAndComForSamosaChicken" name='expAndComForSamosaChicken'
                           value="0">

                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalReceivedForSamosaChicken" name='totalReceivedForSamosaChicken'
                           value="0" readonly>
                </td>
            </tr>

            <tr>
                <td>12p@45...</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForSamosaChicken12p45"
                           name='distributedPacketForSamosaChicken12p45'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForSamosaChicken12p45" name='bonusForSamosaChicken12p45'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForSamosaChicken12p45" name='replaceForSamosaChicken12p45'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForSamosaChicken12p45" name='returnForSamosaChicken12p45'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForSamosaChicken12p45"
                           name='totalSalePacketForSamosaChicken12p45'
                           value="0" readonly>
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForSamosaChicken12p45"
                           name='salesInAmountForSamosaChicken12p45'
                           value="0" readonly>
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSalesForSamosaChicken12p45" name='extSalesForSamosaChicken12p45'
                           value="0">
                </td>
            </tr>

            <!--            *******************SINGARA*****************************************************-->
            <tr>
                <th rowspan="2">SINGARA</th>
                <td>18p@50</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForSingara18p50tk"
                           name='distributedPacketForSingara18p50tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForSingara18p50tk" name='bonusForSingara18p50tk'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForSingara18p50tk" name='replaceForSingara18p50tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForSingara18p50tk" name='returnForSingara18p50tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForSingara18p50tk"
                           name='totalSalePacketForSingara18p50tk'
                           value="0" readonly>
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForSingara18p50tk"
                           name='salesInAmountForSingara18p50tk'
                           value="0" readonly>
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSaleForSingara18p50tk" name='extSaleForSingara18p50tk'
                           value="0">
                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalSaleAmountForSingara" name='totalSaleAmountForSingara'
                           value="0" readonly>

                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueReceivedForSingara" name='dueReceivedForSingara'
                           value="0">

                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueInAmountForSingara" name='dueInAmountForSingara'
                           value="0">

                </td>
                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="expAndComForSingara" name='expAndComForSingara'
                           value="0">

                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalReceivedForSingara" name='totalReceivedForSingara'
                           value="0" readonly>
                </td>
            </tr>

            <tr>
                <td>24p@45tk...</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForSingara12p45tk"
                           name='distributedPacketForSingara12p45tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForSingara12p45tk" name='bonusForSingara12p45tk'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForSingara12p45tk" name='replaceForSingara12p45tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForSingara12p45tk" name='returnForSingara12p45tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForSingara12p45tk"
                           name='totalSalePacketForSingara12p45tk'
                           value="0" readonly>
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForSingara12p45tk"
                           name='salesInAmountForSingara12p45tk'
                           value="0" readonly>
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSalesForSingara12p45tk" name='extSalesForSingara12p45tk'
                           value="0">
                </td>
            </tr>
            <!--*****************************CHICKEN SPRING ROLL**********************************************-->
            <tr>
                <th rowspan="2">CHICKEN SPRING ROLL</th>
                <td>18p@95</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForChickenRoll18p95tk"
                           name='distributedPacketForChickenRoll18p95tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForChickenRoll18p95tk" name='bonusForChickenRoll18p95tk'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForChickenRoll18p95tk" name='replaceForChickenRoll18p95tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForChickenRoll18p95tk" name='returnForChickenRoll18p95tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForChickenRoll18p95tk"
                           name='totalSalePacketForChickenRoll18p95tk'
                           value="0" readonly>
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForChickenRoll18p95tk"
                           name='salesInAmountForChickenRoll18p95tk'
                           value="0" readonly>
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSaleForChickenRoll18p95tk" name='extSaleForChickenRoll18p95tk'
                           value="0">
                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalSaleAmountForChickenRoll" name='totalSaleAmountForChickenRoll'
                           value="0" readonly>
                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueReceivedForChickenRoll" name='dueReceivedForChickenRoll'
                           value="0">

                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueInAmountForChickenRoll" name='dueInAmountForChickenRoll'
                           value="0">

                </td>
                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="expAndComForChickenRoll" name='expAndComForChickenRoll'
                           value="0">

                </td>
                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="totalReceivedForChickenRoll" name='totalReceivedForChickenRoll'
                           value="0" readonly>
                </td>
            </tr>

            <tr>
                <td>24p@45...</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForChickenRoll24p45"
                           name='distributedPacketForChickenRoll24p45'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForChickenRoll24p45" name='bonusForChickenRoll24p45'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForChickenRoll24p45" name='replaceForChickenRoll24p45'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForChickenRoll24p45" name='returnForChickenRoll24p45'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForChickenRoll24p45"
                           name='totalSalePacketForChickenRoll24p45'
                           value="0" readonly>
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForChickenRoll24p45"
                           name='salesInAmountForChickenRoll24p45'
                           value="0" readonly>
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSalesForChickenRoll24p45" name='extSalesForChickenRoll24p45'
                           value="0">
                </td>
            </tr>
            <!--***************************VEGETABLE SPRING ROLL****************************-->
            <tr>
                <th rowspan="2">VEGETABLE SPRING ROLL</th>
                <td>18p@70</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForVegRoll18p70tk"
                           name='distributedPacketForVegRoll18p70tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForVegRoll18p70tk" name='bonusForVegRoll18p70tk'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForVegRoll18p70tk" name='replaceForVegRoll18p70tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForVegRoll18p70tk" name='returnForVegRoll18p70tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForVegRoll18p70tk"
                           name='totalSalePacketForVegRoll18p70tk'
                           value="0" readonly>
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForVegRoll18p70tk"
                           name='salesInAmountForVegRoll18p70tk'
                           value="0" readonly>
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSaleForVegRoll18p70tk" name='extSaleForVegRoll18p70tk'
                           value="0">
                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalSaleAmountForVegRoll" name='totalSaleAmountForVegRoll'
                           value="0" readonly>
                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueReceivedForVegRoll" name='dueReceivedForVegRoll'
                           value="0">

                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueInAmountForVegRoll" name='dueInAmountForVegRoll'
                           value="0">

                </td>
                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="expAndComForVegRoll" name='expAndComForVegRoll'
                           value="0">

                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalReceivedForVegRoll" name='totalReceivedForVegRoll'
                           value="0" readonly>
                </td>
            </tr>

            <tr>
                <td>....@.....</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForVegRollUndefined"
                           name='distributedPacketForVegRollUndefined'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForVegRollUndefined" name='bonusForVegRollUndefined'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForVegRollUndefined" name='replaceForVegRollUndefined'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForVegRollUndefined" name='returnForVegRollUndefined'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForVegRollUndefined"
                           name='totalSalePacketForVegRollUndefined'
                           value="0" readonly>
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForVegRollUndefined"
                           name='salesInAmountForVegRollUndefined'
                           value="0" readonly>
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSalesForVegRollUndefined" name='extSalesForVegRollUndefined'
                           value="0">
                </td>
            </tr>

            <!--***********************************************************************-->
            <tr>
                <th rowspan="2">PUFFED RICE</th>
                <td>.2kg@13</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForPuffedRice2kg13tk"
                           name='distributedPacketForPuffedRice2kg13tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForPuffedRice2kg13tk" name='bonusForPuffedRice2kg13tk'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForPuffedRice2kg13tk" name='replaceForPuffedRice2kg13tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForPuffedRice2kg13tk" name='returnForPuffedRice2kg13tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForPuffedRice2kg13tk"
                           name='totalSalePacketForPuffedRice2kg13tk'
                           value="0" readonly>
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForPuffedRice2kg13tk"
                           name='salesInAmountForPuffedRice2kg13tk'
                           value="0" readonly>
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSaleForPuffedRice2kg13tk" name='extSaleForPuffedRice2kg13tk'
                           value="0">
                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalSaleAmountForPuffedRice" name='totalSaleAmountForPuffedRice'
                           value="0" readonly>
                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueReceivedForPuffedRice" name='dueReceivedForPuffedRice'
                           value="0">

                </td>

                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="dueInAmountForPuffedRice" name='dueInAmountForPuffedRice'
                           value="0">

                </td>
                <td rowspan="2">

                    <input type="text"
                           class="form-control" id="expAndComForPuffedRice" name='expAndComForPuffedRice'
                           value="0">

                </td>
                <td rowspan="2">
                    <input type="text"
                           class="form-control" id="totalReceivedForPuffedRice" name='totalReceivedForPuffedRice'
                           value="0" readonly>
                </td>
            </tr>

            <tr>
                <td>.1kg@07</td>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketForPuffedRice1kg7tk"
                           name='distributedPacketForPuffedRice1kg7tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="bonusForPuffedRice1kg7tk" name='bonusForPuffedRice1kg7tk'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id="replaceForPuffedRice1kg7tk" name='replaceForPuffedRice1kg7tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="returnForPuffedRice1kg7tk" name='returnForPuffedRice1kg7tk'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="totalSalePacketForPuffedRice1kg7tk"
                           name='totalSalePacketForPuffedRice1kg7tk'
                           value="0" readonly>
                </td>

                <td>
                    <input type="text"
                           class="form-control" id="salesInAmountForPuffedRice1kg7tk"
                           name='salesInAmountForPuffedRice1kg7tk'
                           value="0" readonly>
                </td>
                <td>
                    <input type="text"
                           class="form-control" id="extSalesForPuffedRice1kg7tk" name='extSalesForPuffedRice1kg7tk'
                           value="0">
                </td>
            </tr>

            <tr>
                <th rowspan="2"></th>
                <td>....@.....</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td>....@.....</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>


            <tr>
                <th colspan="2">TOTAL</th>
                <td>
                    <input type="text"
                           class="form-control" id="distributedPacketTOTAL" name='distributedPacketTOTAL'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id='bonusTOTAL' name='bonusTOTAL'
                           value="0">
                </td>
                <td><input type="text"
                           class="form-control" id='replaceTOTAL' name='replaceTOTAL'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id='returnTOTAL' name='returnTOTAL'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id='totalSalesInPacketTOTAL' name='totalSalesInPacketTOTAL'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id='salesInAmountTOTAL' name='salesInAmountTOTAL'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id='extSalesTOTAL' name='extSalesTOTAL'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id='totalSalesInAmount' name='totalSalesInAmount'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id='dueReceivedTOTAL' name='dueReceivedTOTAL'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id='dueAmountTOTAL' name='dueAmountTOTAL'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id='expAndComTOTAL' name='exp&expAndComTOTAL'
                           value="0">
                </td>
                <td>
                    <input type="text"
                           class="form-control" id='totalReceivedTOTAL' name='totalReceivedTOTAL'
                           value="0">
                </td>
            </tr>

            <tr>
                <td colspan="9">Ext.</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td colspan="9">Ext.</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td colspan="9">Ext.</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <th colspan="9">GRAND TOTAL RECEIVE:</th>
                <td colspan="5">
                    <input type="text"
                           class="form-control" name='GRANDTOTAL'
                           value="">
                </td>
            </tr>


        </table>
    </form>
</div>


<script>


    function sumForFields(totalSalePacket, salesInAmount, inputids, price) {
        $(inputids.join(',')).on("keydown keyup", function () {
            var total = Number($(inputids[0]).val());

            for (var i = 1; i < 4; i++) {
                total -= Number($(inputids[i]).val());
            }

            $(totalSalePacket).val(total);
            $(salesInAmount).val(total * price);
        });
    }


    // ROTI
    sumForFields('#totalSalePacketForRoti6p20tk', '#salesInAmountForRoti6p20tk',
        ['#distributedPacketForRoti6p20tk', '#bonusForRoti6p20tk', '#replaceForRoti6p20tk',
            '#returnForRoti6p20tk'], 20);

    sumForFields('#totalSalePacketForRoti4p14tk', '#salesInAmountForRoti4p14tk',
        ['#distributedPacketForRoti4p14tk', '#bonusForRoti4p14tk', '#replaceForRoti4p14tk',
            '#returnForRoti4p14tk'], 14);

    // DONUT
    sumForFields('#totalSalePacketForDonut2p18tk', '#salesInAmountForDonut2p18tk',
        ['#distributedPacketForDonut2p18tk', '#bonusForDonut2p18tk', '#replaceForDonut2p18tk',
            '#returnForDonut2p18tk'], 18);

    sumForFields('#totalSalePacketForDonut3p22tk', '#salesInAmountForDonut3p22tk',
        ['#distributedPacketForDonut3p22tk', '#bonusForDonut3p22tk', '#replaceForDonut3p22tk',
            '#returnForDonut3p22tk'], 22);

    // SOMOSA (BEEF)
    sumForFields('#totalSalePacketForSamosaBeef24p45tk', '#salesInAmountForSamosaBeef24p45tk',
        ['#distributedPacketForSamosaBeef24p45tk', '#bonusForSamosaBeef24p45tk', '#replaceForSamosaBeef24p45tk',
            '#returnForSamosaBeef24p45tk'], 45);

    sumForFields('#totalSalePacketForSamosaBeef12p45tk', '#salesInAmountForSamosaBeef12p45tk',
        ['#distributedPacketForSamosaBeef12p45tk', '#bonusForSamosaBeef12p45tk', '#replaceForSamosaBeef12p45tk',
            '#returnForSamosaBeef12p45tk'], 45);

    //  SOMOSA (CHICKEN)
    sumForFields('#totalSalePacketForSamosaChicken24p80tk', '#salesInAmountForSamosaChicken24p80tk',
        ['#distributedPacketForSamosaChicken24p80tk', '#bonusForSamosaChicken24p80tk', '#replaceForSamosaChicken24p80tk',
            '#returnForSamosaChicken24p80tk'], 80);

    sumForFields('#totalSalePacketForSamosaChicken12p45', '#salesInAmountForSamosaChicken12p45',
        ['#distributedPacketForSamosaChicken12p45', '#bonusForSamosaChicken12p45', '#replaceForSamosaChicken12p45',
            '#returnForSamosaChicken12p45'], 45);

    // SINGARA
    sumForFields('#totalSalePacketForSingara18p50tk', '#salesInAmountForSingara18p50tk',
        ['#distributedPacketForSingara18p50tk', '#bonusForSingara18p50tk', '#replaceForSingara18p50tk',
            '#returnForSingara18p50tk'], 50);

    sumForFields('#totalSalePacketForSingara12p45tk', '#salesInAmountForSingara12p45tk',
        ['#distributedPacketForSingara12p45tk', '#bonusForSingara12p45tk', '#replaceForSingara12p45tk',
            '#returnForSingara12p45tk'], 45);

    // CHICKEN SPRING ROLL
    sumForFields('#totalSalePacketForChickenRoll18p95tk', '#salesInAmountForChickenRoll18p95tk',
        ['#distributedPacketForChickenRoll18p95tk', '#bonusForChickenRoll18p95tk', '#replaceForChickenRoll18p95tk',
            '#returnForChickenRoll18p95tk'], 95);

    sumForFields('#totalSalePacketForChickenRoll24p45', '#salesInAmountForChickenRoll24p45',
        ['#distributedPacketForChickenRoll24p45', '#bonusForChickenRoll24p45', '#replaceForChickenRoll24p45',
            '#returnForChickenRoll24p45'], 45);

    // VEGETABLE SPRING ROLL
    sumForFields('#totalSalePacketForVegRoll18p70tk', '#salesInAmountForVegRoll18p70tk',
        ['#distributedPacketForVegRoll18p70tk', '#bonusForVegRoll18p70tk', '#replaceForVegRoll18p70tk',
            '#returnForVegRoll18p70tk'], 70);

    sumForFields('#totalSalePacketForVegRollUndefined', '#salesInAmountForVegRollUndefined',
        ['#distributedPacketForVegRollUndefined', '#bonusForVegRollUndefined', '#replaceForVegRollUndefined',
            '#returnForVegRollUndefined'],
        45);

    // PUFFED RICE
    sumForFields('#totalSalePacketForPuffedRice2kg13tk', '#salesInAmountForPuffedRice2kg13tk',
        ['#distributedPacketForPuffedRice2kg13tk', '#bonusForPuffedRice2kg13tk', '#replaceForPuffedRice2kg13tk',
            '#returnForPuffedRice2kg13tk'], 13);

    sumForFields('#totalSalePacketForPuffedRice1kg7tk', '#salesInAmountForPuffedRice1kg7tk',
        ['#distributedPacketForPuffedRice1kg7tk', '#bonusForPuffedRice1kg7tk', '#replaceForPuffedRice1kg7tk',
            '#returnForPuffedRice1kg7tk'], 7);


    function finalSumForFields(idsOfExtSalesAndSalesInAmount, totalSaleAmount,
                               inputIDs, totalReceive) {
        $(idsOfExtSalesAndSalesInAmount.join(',')).on("keydown keyup", function () {
            var total = 0;
            for (var i = 0; i < idsOfExtSalesAndSalesInAmount.length; i++) {
                total += Number($(idsOfExtSalesAndSalesInAmount[i]).val());
            }

            $(totalSaleAmount).val(total);
        });

        $(inputIDs.join(',')).on("keydown keyup", function () {
            $(totalReceive).val(Number($(totalSaleAmount).val())
                + Number($(inputIDs[0]).val())
                - Number($(inputIDs[1]).val())
                - Number($(inputIDs[2]).val())
            );
        });
    }

    // ROTI
    finalSumForFields(['#extSaleForRoti6p20tk',
            '#extSalesForRoti4p14tk', '#salesInAmountForRoti6p20tk',
            '#salesInAmountForRoti4p14tk'], '#totalSaleAmountForRoti',
        ["#dueReceivedForRoti", "#dueInAmountForRoti", "#expAndComForRoti"],
        "#totalReceivedForRoti");

    // DONUT
    finalSumForFields(['#extSaleForDonut2p18tk',
            '#extSalesForDonut3p22tk', '#salesInAmountForDonut2p18tk',
            '#salesInAmountForDonut3p22tk'], '#totalSaleAmountForDonut',
        ["#dueReceivedForDonut", "#dueInAmountForDonut", "#expAndComForDonut"],
        "#totalReceivedForDonut");

    // Samosa  Beef
    finalSumForFields(['#extSaleForSamosaBeef24p45tk',
            '#extSalesForSamosaBeef12p45tk', '#salesInAmountForSamosaBeef24p45tk',
            '#salesInAmountForSamosaBeef12p45tk'], '#totalSaleAmountForSamosaBeef',
        ["#dueReceivedForSamosaBeef", "#dueInAmountForSamosaBeef", "#expAndComForSamosaBeef"],
        "#totalReceivedForSamosaBeef");


    // Samosa Chicken
    finalSumForFields(['#extSaleForSamosaChicken24p80tk',
            '#extSalesForSamosaChicken12p45', '#salesInAmountForSamosaChicken24p80tk',
            '#salesInAmountForSamosaChicken12p45'], '#totalSaleAmountForSamosaChicken',
        ["#dueReceivedForSamosaChicken", "#dueInAmountForSamosaChicken", "#expAndComForSamosaChicken"],
        "#totalReceivedForSamosaChicken");


    // Singara
    finalSumForFields(['#extSaleForSingara18p50tk',
            '#extSalesForSingara12p45tk', '#salesInAmountForSingara18p50tk',
            '#salesInAmountForSingara12p45tk'], '#totalSaleAmountForSingara',
        ["#dueReceivedForSingara", "#dueInAmountForSingara", "#expAndComForSingara"],
        "#totalReceivedForSingara");


    // Chicken Roll
    finalSumForFields(['#extSaleForChickenRoll18p95tk',
            '#extSalesForChickenRoll24p45', '#salesInAmountForChickenRoll18p95tk',
            '#salesInAmountForChickenRoll24p45'], '#totalSaleAmountForChickenRoll',
        ["#dueReceivedForChickenRoll", "#dueInAmountForChickenRoll", "#expAndComForChickenRoll"],
        "#totalReceivedForChickenRoll");


    // Veg Roll
    finalSumForFields(['#extSaleForVegRoll18p70tk',
            '#extSalesForVegRollUndefined', '#salesInAmountForVegRoll18p70tk',
            '#salesInAmountForVegRollUndefined'], '#totalSaleAmountForVegRoll',
        ["#dueReceivedForVegRoll", "#dueInAmountForVegRoll", "#expAndComForVegRoll"],
        "#totalReceivedForVegRoll");


    // Puffed Rice
    finalSumForFields(['#extSaleForPuffedRice2kg13tk',
            '#extSalesForPuffedRice1kg7tk', '#salesInAmountForPuffedRice2kg13tk',
            '#salesInAmountForPuffedRice1kg7tk'], '#totalSaleAmountForPuffedRice',
        ["#dueReceivedForPuffedRice", "#dueInAmountForPuffedRice", "#expAndComForPuffedRice"],
        "#totalReceivedForPuffedRice");


    //TOTAL SUM

    function totalSumForFields(inputIDs, totalPlacementID) {
        $(inputIDs.join(',')).on("keydown keyup", function () {
            var total = 0;
            for (var i = 0; i < inputIDs.length; i++) {
                total += Number($(inputIDs[i]).val());
            }
            $(totalPlacementID).val(total);
        });
    }

    // distributedPacketTOTAL
    totalSumForFields(['#distributedPacketForRoti6p20tk',
            '#distributedPacketForRoti4p14tk',
            '#distributedPacketForDonut2p18tk',
            '#distributedPacketForDonut3p22tk',
            '#distributedPacketForSamosaBeef24p45tk',
            '#distributedPacketForSamosaBeef12p45tk',
            '#distributedPacketForSamosaChicken24p80tk',
            '#distributedPacketForSamosaChicken12p45',
            '#distributedPacketForSingara18p50tk',
            '#distributedPacketForSingara12p45tk',
            '#distributedPacketForChickenRoll18p95tk',
            '#distributedPacketForChickenRoll24p45',
            '#distributedPacketForVegRoll18p70tk',
            '#distributedPacketForVegRollUndefined',
            '#distributedPacketForPuffedRice2kg13tk',
            '#distributedPacketForPuffedRice1kg7tk'],
        '#distributedPacketTOTAL');

    // bonusTOTAL
    totalSumForFields(['#bonusForRoti6p20tk',
            '#bonusForRoti4p14tk',
            '#bonusForDonut2p18tk',
            '#bonusForDonut3p22tk',
            '#bonusForSamosaBeef24p45tk',
            '#bonusForSamosaBeef12p45tk',
            '#bonusForSamosaChicken24p80tk',
            '#bonusForSamosaChicken12p45',
            '#bonusForSingara18p50tk',
            '#bonusForSingara12p45tk',
            '#bonusForChickenRoll18p95tk',
            '#bonusForChickenRoll24p45',
            '#bonusForVegRoll18p70tk',
            '#bonusForVegRollUndefined',
            '#bonusForPuffedRice2kg13tk',
            '#bonusForPuffedRice1kg7tk'],
        '#bonusTOTAL');

    //replaceTOTAL
    totalSumForFields(['#replaceForRoti6p20tk',
            '#replaceForRoti4p14tk',
            '#replaceForDonut2p18tk',
            '#replaceForDonut3p22tk',
            '#replaceForSamosaBeef24p45tk',
            '#replaceForSamosaBeef12p45tk',
            '#replaceForSamosaChicken24p80tk',
            '#replaceForSamosaChicken12p45',
            '#replaceForSingara18p50tk',
            '#replaceForSingara12p45tk',
            '#replaceForChickenRoll18p95tk',
            '#replaceForChickenRoll24p45',
            '#replaceForVegRoll18p70tk',
            '#replaceForVegRollUndefined',
            '#replaceForPuffedRice2kg13tk',
            '#replaceForPuffedRice1kg7tk'],
        '#replaceTOTAL');

    //returnTOTAL
    totalSumForFields(['#returnForRoti6p20tk',
            '#returnForRoti4p14tk',
            '#returnForDonut2p18tk',
            '#returnForDonut3p22tk',
            '#returnForSamosaBeef24p45tk',
            '#returnForSamosaBeef12p45tk',
            '#returnForSamosaChicken24p80tk',
            '#returnForSamosaChicken12p45',
            '#returnForSingara18p50tk',
            '#returnForSingara12p45tk',
            '#returnForChickenRoll18p95tk',
            '#returnForChickenRoll24p45',
            '#returnForVegRoll18p70tk',
            '#returnForVegRollUndefined',
            '#returnForPuffedRice2kg13tk',
            '#returnForPuffedRice1kg7tk'],
        '#returnTOTAL');

    //totalSalesInPacketTOTAL
    totalSumForFields(['#totalSalePacketForRoti6p20tk',
            '#totalSalePacketForRoti4p14tk',
            '#totalSalePacketForDonut2p18tk',
            '#totalSalePacketForDonut3p22tk',
            '#totalSalePacketForSamosaBeef24p45tk',
            '#totalSalePacketForSamosaBeef12p45tk',
            '#totalSalePacketForSamosaChicken24p80tk',
            '#totalSalePacketForSamosaChicken12p45',
            '#totalSalePacketForSingara18p50tk',
            '#totalSalePacketForSingara12p45tk',
            '#totalSalePacketForChickenRoll18p95tk',
            '#totalSalePacketForChickenRoll24p45',
            '#totalSalePacketForVegRoll18p70tk',
            '#totalSalePacketForVegRollUndefined',
            '#totalSalePacketForPuffedRice2kg13tk',
            '#totalSalePacketForPuffedRice1kg7tk'],
        '#totalSalesInPacketTOTAL');

    //salesInAmountTOTAL
    totalSumForFields(['#salesInAmountForRoti6p20tk',
            '#salesInAmountForRoti4p14tk',
            '#salesInAmountForDonut2p18tk',
            '#salesInAmountForDonut3p22tk',
            '#salesInAmountForSamosaBeef24p45tk',
            '#salesInAmountForSamosaBeef12p45tk',
            '#salesInAmountForSamosaChicken24p80tk',
            '#salesInAmountForSamosaChicken12p45',
            '#salesInAmountForSingara18p50tk',
            '#salesInAmountForSingara12p45tk',
            '#salesInAmountForChickenRoll18p95tk',
            '#salesInAmountForChickenRoll24p45',
            '#salesInAmountForVegRoll18p70tk',
            '#salesInAmountForVegRollUndefined',
            '#salesInAmountForPuffedRice2kg13tk',
            '#salesInAmountForPuffedRice1kg7tk'],
        '#salesInAmountTOTAL');

    //extSalesTOTAL
    totalSumForFields(['#extSaleForRoti6p20tk',
            '#extSalesForRoti4p14tk',
            '#extSaleForDonut2p18tk',
            '#extSalesForDonut3p22tk',
            '#extSaleForSamosaBeef24p45tk',
            '#extSalesForSamosaBeef12p45tk',
            '#extSaleForSamosaChicken24p80tk',
            '#extSalesForSamosaChicken12p45',
            '#extSaleForSingara18p50tk',
            '#extSalesForSingara12p45tk',
            '#extSaleForChickenRoll18p95tk',
            '#extSalesForChickenRoll24p45',
            '#extSaleForVegRoll18p70tk',
            '#extSalesForVegRollUndefined',
            '#extSaleForPuffedRice2kg13tk',
            '#extSalesForPuffedRice1kg7tk'],
        '#extSalesTOTAL');

    //totalSalesInAmount
    totalSumForFields(['#totalSaleAmountForRoti',
            '#totalSaleAmountForDonut',
            '#totalSaleAmountForSamosaBeef',
            '#totalSaleAmountForSamosaChicken',
            '#totalSaleAmountForSingara',
            '#totalSaleAmountForChickenRoll',
            '#totalSaleAmountForVegRoll',
            '#totalSaleAmountForPuffedRice'],
        '#totalSalesInAmount');

    //dueReceivedTOTAL
    totalSumForFields(['#dueReceivedForRoti',
            '#dueReceivedForDonut',
            '#dueReceivedForSamosaBeef',
            '#dueReceivedForSamosaChicken',
            '#dueReceivedForSingara',
            '#dueReceivedForChickenRoll',
            '#dueReceivedForVegRoll',
            '#dueReceivedForPuffedRice'],
        '#dueReceivedTOTAL');

    //dueAmountTOTAL
    totalSumForFields(['#dueInAmountForRoti',
            '#dueInAmountForDonut',
            '#dueInAmountForSamosaBeef',
            '#dueInAmountForSamosaChicken',
            '#dueInAmountForSingara',
            '#dueInAmountForChickenRoll',
            '#dueInAmountForVegRoll',
            '#dueInAmountForPuffedRice'],
        '#dueAmountTOTAL');

    //expAndComTOTAL
    totalSumForFields(['#expAndComForRoti',
            '#expAndComForDonut',
            '#expAndComForSamosaBeef',
            '#expAndComForSamosaChicken',
            '#expAndComForSingara',
            '#expAndComForChickenRoll',
            '#expAndComForVegRoll',
            '#expAndComForPuffedRice'],
        '#expAndComTOTAL');

    //totalReceivedTOTAL
    totalSumForFields(['#totalReceivedForRoti',
            '#totalReceivedForDonut',
            '#totalReceivedForSamosaBeef',
            '#totalReceivedForSamosaChicken',
            '#totalReceivedForSingara',
            '#totalReceivedForChickenRoll',
            '#totalReceivedForVegRoll',
            '#totalReceivedForPuffedRice'],
        '#totalReceivedTOTAL');
</script>


</body>