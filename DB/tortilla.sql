-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2016 at 05:41 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tortilla`
--

-- --------------------------------------------------------

--
-- Table structure for table `daily_sales_report`
--

CREATE TABLE `daily_sales_report` (
  `id` int(11) NOT NULL,
  `distributedPacketForRoti6p20tk` int(8) NOT NULL,
  `bonusForRoti6p20tk` int(8) NOT NULL,
  `replaceForRoti6p20tk` int(8) NOT NULL,
  `returnForRoti6p20tk` int(8) NOT NULL,
  `totalSalePacketForRoti6p20tk` int(8) NOT NULL,
  `salesInAmountForRoti6p20tk` int(8) NOT NULL,
  `extSaleForRoti6p20tk` int(8) NOT NULL,
  `totalSaleAmountForRoti` int(8) NOT NULL,
  `dueReceivedForRoti` int(8) NOT NULL,
  `dueInAmountForRoti` int(8) NOT NULL,
  `expAndComForRoti` int(8) NOT NULL,
  `totalReceivedForRoti` int(8) NOT NULL,
  `distributedPacketForRoti4p14tk` int(8) NOT NULL,
  `bonusForRoti4p14tk` int(8) NOT NULL,
  `replaceForRoti4p14tk` int(8) NOT NULL,
  `returnForRoti4p14tk` int(8) NOT NULL,
  `totalSalePacketForRoti4p14tk` int(8) NOT NULL,
  `salesInAmountForRoti4p14tk` int(8) NOT NULL,
  `extSalesForRoti4p14tk` int(8) NOT NULL,
  `distributedPacketForDonut2p18tk` int(8) NOT NULL,
  `bonusForDonut2p18tk` int(8) NOT NULL,
  `replaceForDonut2p18tk` int(8) NOT NULL,
  `returnForDonut2p18tk` int(8) NOT NULL,
  `totalSalePacketForDonut2p18tk` int(8) NOT NULL,
  `salesInAmountForDonut2p18tk` int(8) NOT NULL,
  `extSaleForDonut2p18tk` int(8) NOT NULL,
  `totalSaleAmountForDonut` int(8) NOT NULL,
  `dueReceivedForDonut` int(8) NOT NULL,
  `dueInAmountForDonut` int(8) NOT NULL,
  `expAndComForDonut` int(8) NOT NULL,
  `totalReceivedForDonut` int(8) NOT NULL,
  `distributedPacketForDonut3p22tk` int(8) NOT NULL,
  `bonusForDonut3p22tk` int(8) NOT NULL,
  `replaceForDonut3p22tk` int(8) NOT NULL,
  `returnForDonut3p22tk` int(8) NOT NULL,
  `totalSalePacketForDonut3p22tk` int(8) NOT NULL,
  `salesInAmountForDonut3p22tk` int(8) NOT NULL,
  `extSalesForDonut3p22tk` int(8) NOT NULL,
  `distributedPacketForSamosaBeef24p45tk` int(11) NOT NULL,
  `bonusForSamosaBeef24p45tk` int(11) NOT NULL,
  `replaceForSamosaBeef24p45tk` int(11) NOT NULL,
  `returnForSamosaBeef24p45tk` int(11) NOT NULL,
  `totalSalePacketForSamosaBeef24p45tk` int(11) NOT NULL,
  `salesInAmountForSamosaBeef24p45tk` int(11) NOT NULL,
  `extSaleForSamosaBeef24p45tk` int(11) NOT NULL,
  `totalSaleAmountForSamosaBeef` int(11) NOT NULL,
  `dueReceivedForSamosaBeef` int(11) NOT NULL,
  `dueInAmountForSamosaBeef` int(11) NOT NULL,
  `expAndComForSamosaBeef` int(11) NOT NULL,
  `totalReceivedForSamosaBeef` int(11) NOT NULL,
  `distributedPacketForSamosaBeef12p45tk` int(11) NOT NULL,
  `bonusForSamosaBeef12p45tk` int(11) NOT NULL,
  `replaceForSamosaBeef12p45tk` int(11) NOT NULL,
  `returnForSamosaBeef12p45tk` int(11) NOT NULL,
  `totalSalePacketForSamosaBeef12p45tk` int(11) NOT NULL,
  `salesInAmountForSamosaBeef12p45tk` int(11) NOT NULL,
  `extSalesForSamosaBeef12p45tk` int(11) NOT NULL,
  `distributedPacketForSamosaChicken24p80tk` int(11) NOT NULL,
  `bonusForSamosaChicken24p80tk` int(11) NOT NULL,
  `replaceForSamosaChicken24p80tk` int(11) NOT NULL,
  `returnForSamosaChicken24p80tk` int(11) NOT NULL,
  `totalSalePacketForSamosaChicken24p80tk` int(11) NOT NULL,
  `salesInAmountForSamosaChicken24p80tk` int(11) NOT NULL,
  `extSaleForSamosaChicken24p80tk` int(11) NOT NULL,
  `totalSaleAmountForSamosaChicken` int(11) NOT NULL,
  `dueReceivedForSamosaChicken` int(11) NOT NULL,
  `dueInAmountForSamosaChicken` int(11) NOT NULL,
  `expAndComForSamosaChicken` int(11) NOT NULL,
  `totalReceivedForSamosaChicken` int(11) NOT NULL,
  `distributedPacketForSamosaChicken12p45` int(11) NOT NULL,
  `bonusForSamosaChicken12p45` int(11) NOT NULL,
  `replaceForSamosaChicken12p45` int(11) NOT NULL,
  `returnForSamosaChicken12p45` int(11) NOT NULL,
  `totalSalePacketForSamosaChicken12p45` int(11) NOT NULL,
  `salesInAmountForSamosaChicken12p45` int(11) NOT NULL,
  `extSalesForSamosaChicken12p45` int(11) NOT NULL,
  `distributedPacketForSingara18p50tk` int(11) NOT NULL,
  `bonusForSingara18p50tk` int(11) NOT NULL,
  `replaceForSingara18p50tk` int(11) NOT NULL,
  `returnForSingara18p50tk` int(11) NOT NULL,
  `totalSalePacketForSingara18p50tk` int(11) NOT NULL,
  `salesInAmountForSingara18p50tk` int(11) NOT NULL,
  `extSaleForSingara18p50tk` int(11) NOT NULL,
  `totalSaleAmountForSingara` int(11) NOT NULL,
  `dueReceivedForSingara` int(11) NOT NULL,
  `dueInAmountForSingara` int(11) NOT NULL,
  `expAndComForSingara` int(11) NOT NULL,
  `totalReceivedForSingara` int(11) NOT NULL,
  `distributedPacketForSingara24p45tk` int(11) NOT NULL,
  `bonusForSingara24p45tk` int(11) NOT NULL,
  `replaceForSingara24p45tk` int(11) NOT NULL,
  `returnForSingara24p45tk` int(11) NOT NULL,
  `totalSalePacketForSingara24p45tk` int(11) NOT NULL,
  `salesInAmountForSingara24p45tk` int(11) NOT NULL,
  `extSalesForSingara24p45tk` int(11) NOT NULL,
  `distributedPacketForChickenRoll18p95tk` int(11) NOT NULL,
  `bonusForChickenRoll18p95tk` int(11) NOT NULL,
  `replaceForChickenRoll18p95tk` int(11) NOT NULL,
  `returnForChickenRoll18p95tk` int(11) NOT NULL,
  `totalSalePacketForChickenRoll18p95tk` int(11) NOT NULL,
  `salesInAmountForChickenRoll18p95tk` int(11) NOT NULL,
  `extSaleForChickenRoll18p95t` int(11) NOT NULL,
  `totalSaleAmountForChickenRoll` int(11) NOT NULL,
  `dueReceivedForChickenRoll` int(11) NOT NULL,
  `dueInAmountForChickenRoll` int(11) NOT NULL,
  `expAndComForChickenRoll` int(11) NOT NULL,
  `totalReceivedForChickenRoll` int(11) NOT NULL,
  `distributedPacketForChickenRoll12p45` int(11) NOT NULL,
  `bonusForChickenRoll12p45` int(11) NOT NULL,
  `replaceForChickenRoll12p45` int(11) NOT NULL,
  `returnForChickenRoll12p45` int(11) NOT NULL,
  `totalSalePacketForChickenRoll12p45` int(11) NOT NULL,
  `salesInAmountForChickenRoll12p45` int(11) NOT NULL,
  `extSalesForChickenRoll12p45` int(11) NOT NULL,
  `distributedPacketForVegRoll18p70tk` int(11) NOT NULL,
  `bonusForVegRoll18p70tk` int(11) NOT NULL,
  `replaceForVegRoll18p70tk` int(11) NOT NULL,
  `returnForVegRoll18p70tk` int(11) NOT NULL,
  `totalSalePacketForVegRoll18p70tk` int(11) NOT NULL,
  `salesInAmountForVegRoll18p70tk` int(11) NOT NULL,
  `extSaleForVegRoll18p70tk` int(11) NOT NULL,
  `totalSaleAmountForVegRoll` int(11) NOT NULL,
  `dueReceivedForVegRoll` int(11) NOT NULL,
  `dueInAmountForVegRoll` int(11) NOT NULL,
  `expAndComForVegRoll` int(11) NOT NULL,
  `totalReceivedForVegRoll` int(11) NOT NULL,
  `distributedPacketForVegRollUndefined` int(11) NOT NULL,
  `bonusForVegRollUndefined` int(11) NOT NULL,
  `replaceForVegRollUndefined` int(11) NOT NULL,
  `returnForVegRollUndefined` int(11) NOT NULL,
  `totalSalePacketForVegRollUndefined` int(11) NOT NULL,
  `salesInAmountForVegRollUndefined` int(11) NOT NULL,
  `extSalesForVegRollUndefined` int(11) NOT NULL,
  `distributedPacketForPuffedRice2kg13tk` int(11) NOT NULL,
  `bonusForPuffedRice2kg13tk` int(11) NOT NULL,
  `replaceForPuffedRice2kg13tk` int(11) NOT NULL,
  `returnForPuffedRice2kg13tk` int(11) NOT NULL,
  `totalSalePacketForPuffedRice2kg13tk` int(11) NOT NULL,
  `salesInAmountForPuffedRice2kg13tk` int(11) NOT NULL,
  `extSaleForPuffedRice2kg13tk` int(11) NOT NULL,
  `totalSaleAmountForPuffedRice` int(11) NOT NULL,
  `dueReceivedForPuffedRice` int(11) NOT NULL,
  `dueInAmountForPuffedRice` int(11) NOT NULL,
  `expAndComForPuffedRice` int(11) NOT NULL,
  `totalReceivedForPuffedRice` int(11) NOT NULL,
  `distributedPacketForPuffedRice1kg7tk` int(11) NOT NULL,
  `bonusForPuffedRice1kg7tk` int(11) NOT NULL,
  `replaceForPuffedRice1kg7tk` int(11) NOT NULL,
  `returnForPuffedRice1kg7tk` int(11) NOT NULL,
  `totalSalePacketForPuffedRice1kg7tk` int(11) NOT NULL,
  `salesInAmountForPuffedRice1kg7tk` int(11) NOT NULL,
  `extSalesForPuffedRice1kg7tk` int(11) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daily_sales_report`
--
ALTER TABLE `daily_sales_report`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daily_sales_report`
--
ALTER TABLE `daily_sales_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
